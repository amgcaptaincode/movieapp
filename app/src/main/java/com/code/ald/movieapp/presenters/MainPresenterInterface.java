package com.code.ald.movieapp.presenters;

public interface MainPresenterInterface {

    void getMovies();

}
