package com.code.ald.movieapp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.code.ald.movieapp.R;
import com.code.ald.movieapp.models.Result;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MoviesHolder> {

    List<Result> movieList;
    Context context;

    public MoviesAdapter(List<Result> movieList, Context context) {
        this.movieList = movieList;
        this.context = context;
    }

    @NonNull
    @Override
    public MoviesHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_movies, viewGroup, false);
        MoviesHolder mh = new MoviesHolder(view);
        return mh;
    }

    @Override
    public void onBindViewHolder(@NonNull MoviesHolder moviesHolder, int position) {
        Result result = movieList.get(position);
        moviesHolder.tvTitle.setText(result.getTitle());
        moviesHolder.tvOverview.setText(result.getOverview());
        moviesHolder.tvReleaseDate.setText(result.getReleaseDate());
        Glide.with(context)
                .load("https://image.tmdb.org/t/p/w500/" + result.getPosterPath())
                .into(moviesHolder.ivMovie);
    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }

    public class MoviesHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvTitle) TextView tvTitle;
        @BindView(R.id.tvOverView) TextView tvOverview;
        @BindView(R.id.tvReleaseDate) TextView tvReleaseDate;
        @BindView(R.id.ivMovie) ImageView ivMovie;

        public MoviesHolder(@NonNull View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
