package com.code.ald.movieapp.presenters;

import com.code.ald.movieapp.models.MovieResponse;

public interface MainViewInterface {

    void showToast(String message);
    void displayMovies(MovieResponse movieResponse);
    void displayError(String error);

}
